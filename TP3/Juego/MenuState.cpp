#include "MenuState.h"



MenuState::MenuState() 
{
	if (!al_init_font_addon())
	{
		fprintf(stderr, "Failed to initialize allegro.font\n");
	}
	if (!al_init_ttf_addon())
	{
		fprintf(stderr, "Failed to initialize allegro.ttf\n");
	}
	running = true;
	
}


MenuState::~MenuState()
{
}

void MenuState::Run(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer)
{
	font = al_load_font("OpenSans-Regular.ttf", 50, 0);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_draw_text(font, al_map_rgb(255, 0, 255), 350, 50, 0, "MENU");
	al_draw_text(font, al_map_rgb(255, 0, 255), 50, 250, 0, "Presione S para comenzar");
	al_flip_display();
	GameState Juego;

	while (running)
	{
		ALLEGRO_EVENT event;
		ALLEGRO_TIMEOUT timeout;

		al_init_timeout(&timeout, 0.06);

		bool get_event = al_wait_for_event_until(event_queue, &event, &timeout);

		// MANEJO EVENTOS
		if (get_event)
		{
			switch (event.type)
			{
			case ALLEGRO_EVENT_DISPLAY_CLOSE:
				running = false;
				break;
			case ALLEGRO_EVENT_KEY_DOWN:
				switch (event.keyboard.keycode)
				{
				case ALLEGRO_KEY_S:
					Juego.Run(display,event_queue,timer);
					running = false;
					break;
				case ALLEGRO_KEY_ESCAPE:
					running = false;
					break;
				}
			}

		}
		// REDRAW
		al_clear_to_color(al_map_rgb(0, 0, 0));
		al_draw_text(font, al_map_rgb(255, 0, 255), 350, 50, 0, "MENU");
		al_draw_text(font, al_map_rgb(255, 0, 255), 135, 250, 0, "Presione S para comenzar");
		al_flip_display();
	}
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
}