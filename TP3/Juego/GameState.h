#ifndef GAMESTATE_H
#define GAMESTATE_H
#include <iostream>
#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include "MovingObject.h"
#include "Bullet.h"
class GameState
{
private:
	bool running;
	bool redraw;
public:
	GameState();
	~GameState();
	void Run(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer);
};

#endif
