#ifndef MENUSTATE_H
#define MENUSTATE_H
#include <iostream>
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include "GameState.h"

class MenuState
{
	private:
	ALLEGRO_FONT *font;
	bool running;
	ALLEGRO_COLOR textColor;

	public:
	MenuState();
	~MenuState();
	void Run(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer);
};
#endif