#ifndef MOVINGOBJECT_H
#define MOVINGOBJECT_H
#include <allegro5\allegro.h>
class MovingObject
{
public:
	MovingObject(int x, int y, ALLEGRO_BITMAP *s);
	MovingObject();
	~MovingObject();
	ALLEGRO_BITMAP *sprite;
	int height;
	int width;
	int posX; 
	int posY;
	int moveSpeed;
	int lives;
};


#endif