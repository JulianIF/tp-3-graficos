#ifndef BULLET_H
#define BULLET_H
#include "MovingObject.h"
class Bullet :
	public MovingObject
{
private:
	
public:
	bool exists = false;
	Bullet(int x, int y, char direction, ALLEGRO_BITMAP *s);
	~Bullet();
	void Move();
	ALLEGRO_BITMAP *sprite;
	char dir;
};
#endif

