#include "MovingObject.h"

MovingObject::MovingObject(int x, int y, ALLEGRO_BITMAP *s)
	:
	height(20),
	width(20),
	posX(x),
	posY(y),
	sprite(s),
	moveSpeed(30),
	lives (3)
{

}
MovingObject::MovingObject()
{

}

MovingObject::~MovingObject()
{
	al_destroy_bitmap(sprite);
}