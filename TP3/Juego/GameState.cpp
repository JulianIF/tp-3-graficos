#include "GameState.h"



GameState::GameState()
{
	if (!al_init_image_addon())
	{
		fprintf(stderr, "Failed to initialize allegro.image\n");
	}

	running = true;
	redraw = true;
}


GameState::~GameState()
{
}

void GameState::Run(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer)
{	
	int window_width = 800;
	int window_height = 600;
	// Player
	MovingObject *player = new MovingObject(20, 20, al_load_bitmap("Player.png"));
	char dir = 'j';
	bool shot = false;
	//Enemy
	MovingObject *enemy	 = new MovingObject(0, rand() % window_height, al_load_bitmap("Enemy.png"));
	enemy->moveSpeed = 15;
	bool reset = true;
	//Bala
	Bullet *bala = new Bullet(1000, 1000, dir, al_load_bitmap("Bullet.png"));
	// MUESTRA
	al_draw_bitmap(player->sprite, player->posX, player->posY, 0);
	al_draw_bitmap(enemy->sprite, window_width, rand() % window_height, 0);
	al_flip_display();

	al_start_timer(timer);

	// GAME LOOP
	while (running)
	{
		ALLEGRO_EVENT event;
		ALLEGRO_TIMEOUT timeout;

		al_init_timeout(&timeout, 0.06);

		bool get_event = al_wait_for_event_until(event_queue, &event, &timeout);

		// MANEJO EVENTOS
		if (get_event)
		{
			switch (event.type)
			{
				case ALLEGRO_EVENT_TIMER:
					redraw = true;
					break;
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					running = false;
					break;
				case ALLEGRO_EVENT_KEY_DOWN:
					//PLAYER MOVEMENT
					switch (event.keyboard.keycode)
					{
						case ALLEGRO_KEY_UP:
							if (player->posY > player->moveSpeed)
							player->posY -= player->moveSpeed;
							dir = 'U';
							break;
						case ALLEGRO_KEY_DOWN:
							if (player->posY < window_height - player->moveSpeed - player->height)
							player->posY += player->moveSpeed;
							dir = 'D';
							break;
						case ALLEGRO_KEY_RIGHT:
							if (player->posX < window_width - player->moveSpeed - player->width)
							player->posX += player->moveSpeed;
							dir = 'R';
							break;
						case ALLEGRO_KEY_LEFT:
							if (player->posX > player->moveSpeed)
							player->posX -= player->moveSpeed;
							dir = 'L';
							break;
						case ALLEGRO_KEY_ESCAPE:
							running = false;
							break;
						case ALLEGRO_KEY_SPACE:
							bala->posX = player->posX;
							bala->posY = player->posY;
							bala->dir = dir;
							break;
					}
			}

		}
		//ENEMIGOS
		if (reset)
		{
			enemy->posY = player->posY;
			enemy->posX = window_width - enemy->width;
			reset = false;
		}
		else
		{
			if (enemy->posX == 0)
			reset = true;
			else
			enemy->posX -= enemy->moveSpeed;
		}
		if (bala->exists)
		{
			bala->Move();
		}
		// Collision Player-Enemy
		if ((player->posX >= enemy->posX && player->posX <= enemy->posX + enemy->width) ||
		(player->posX + player->width >= enemy->posX  &&  player->posX + player->width <= enemy->posX + enemy->width))
		{
			if ((player->posY >= enemy->posY &&  player->posY <= enemy->posY + enemy->height) ||
			(player->posY + player->height >= enemy->posY  &&  player->posY + player->height <= enemy->posY + enemy->height))
			{
				if (player->lives <= 0)
					running = false;
				else
				{
					player->lives -= 1;
					reset = true;
				}
			}
		}
		//Collision Bullet-Enemy
		if ((bala->posX >= enemy->posX && bala->posX <= enemy->posX + enemy->width) ||
			(bala->posX + bala->width >= enemy->posX  &&  bala->posX + bala->width <= enemy->posX + enemy->width))
		{
			if ((bala->posY >= enemy->posY &&  bala->posY <= enemy->posY + enemy->height) ||
				(bala->posY + bala->height >= enemy->posY  &&  bala->posY + bala->height <= enemy->posY + enemy->height))
			{
				reset = true;
				bala->posX = 1000;
			}
		}
		
		if (redraw && al_is_event_queue_empty(event_queue))
		{
			// REDRAW
			al_draw_bitmap(player->sprite, player->posX, player->posY, 0);
			al_draw_bitmap(enemy->sprite, enemy->posX, enemy->posY, 0);
			al_draw_bitmap(bala->sprite, bala->posX, bala->posY, 0);
			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
			redraw = false;
		}
	}

	// LIMPIEZA
	delete (player);
	delete (enemy);
}