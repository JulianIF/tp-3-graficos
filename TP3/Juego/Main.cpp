#include <stdio.h>
#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include "MovingObject.h"
#include "MenuState.h"

const float FPS = 60;


int main(int argc, char *argv[])
{
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	MenuState *menu = new MenuState();

	srand(time(NULL));

	bool running = true;
	bool redraw = true;
	

	// INIT ALLEGRO
	if (!al_init()) 
	{
		fprintf(stderr, "Failed to initialize allegro.\n");
		return 1;
	}

	// INIT TIMER
	timer = al_create_timer(1.0 / FPS);
	if (!timer)
	{
		fprintf(stderr, "Failed to create timer.\n");
		return 1;
	}
	
	// CREA DISPLAY
	int window_width = 800;
	int window_height = 600;

	display = al_create_display(window_width, window_height);
	if (!display) 
	{
		fprintf(stderr, "Failed to create display.\n");
		return 1;
	}
	al_set_target_backbuffer(display);
	// CREA EVENT QUEUE
	event_queue = al_create_event_queue();
	if (!event_queue) 
	{
		fprintf(stderr, "Failed to create event queue.");
		return 1;
	}
	al_install_keyboard();

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	// HASTA ACA DEBERIA LLEGAR EL MAIN, LLAMAR A MENUSTATE
	menu->Run(display, event_queue, timer);
	

}