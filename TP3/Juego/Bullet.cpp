#include "Bullet.h"

Bullet::Bullet(int x, int y, char direction, ALLEGRO_BITMAP *s)
	:
	exists(true),
	dir(direction)
{
	height = 4;
	width = 4;
	posX = x;
	posY = y;
	moveSpeed = 15;
	sprite = s;
}


Bullet::~Bullet()
{
	exists = false;
}

void Bullet::Move()
{
	switch (dir)
	{
	case 'D':
		this->posY += this->moveSpeed;
		break;
	case 'U':
		this->posY -= this->moveSpeed;
		break;
	case 'R':
		this->posX += this->moveSpeed;
		break;
	case 'L':
		this->posX -= this->moveSpeed;
		break;
	}
}